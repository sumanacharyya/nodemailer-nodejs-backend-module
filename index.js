require("dotenv").config();
const express = require("express");
const mainRouter = require("./routes/route");

const app = express();
const PORT = process.env.PORT || 8080;
app.use(express.json());

app.use("/api/v1", mainRouter);

app.listen(PORT, () => {
  console.log(`App listening at port --> http://localhost:${PORT}`);
});
