const nodeMailer = require("nodemailer");
const MailGen = require("mailgen");

// sending Email from test account
const signup = async (req, res, next) => {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodeMailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodeMailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: testAccount.user, // generated ethereal user
      pass: testAccount.pass, // generated ethereal password
    },
  });

  // send mail with defined transport object
  let message = {
    from: '"Fred Foo 👻" <foo@example.com>', // sender address
    to: "bar@example.com, baz@example.com", // list of receivers
    subject: "Hello, welcome user ✔", // Subject line
    text: "Registration Successfull!", // plain text body
    html: "<h1><b>Registration Successfull!</b></h1>", // html body
  };

  transporter
    .sendMail(message)
    .then((resp) => {
      return res.status(201).json({
        success: true,
        message: "Please check your inbox for email!",
        mailInfo: resp.messageId,
        mailPreview: nodeMailer.getTestMessageUrl(resp),
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(401).json({
        success: false,
        message: err,
      });
    });
};

// sending Email from real GMail account
const getbill = (req, res, next) => {
  const { userEmail } = req.body;

  let config = {
    service: "gmail",
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  };

  let transporter = nodeMailer.createTransport(config);

  let MailGenerator = new MailGen({
    theme: "default",
    product: {
      name: "Suman Acharyya",
      link: "https://suman-dev.in/",
    },
  });

  let response = {
    body: {
      name: "Nodemail Checking and Pratice",
      intro: "Your bill has arrived!",
      table: {
        data: [
          {
            item: "NodeMailer Stack Book.",
            description: "A NodeMailer Backend Stack Application!",
            price: "$49.99",
          },
          {
            item: "NodeMailer Another Stack Book.",
            description: "A NodeMailer Backend Stack Application anoter!",
            price: "$19.99",
          },
        ],
      },
      outro: "Looking forward to do more services with you!",
    },
  };

  let mail = MailGenerator.generate(response);

  let message = {
    from: process.env.EMAIL,
    to: userEmail,
    subject: "Place Order",
    html: mail,
  };

  console.log(message);

  transporter
    .sendMail(message)
    .then((resp) => {
      return res.status(201).json({
        success: true,
        message: "Email sent to you!",
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(401).json({
        success: false,
        message: err,
      });
    });
};

module.exports = {
  signup,
  getbill,
};
